#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"
#include <stdbool.h>

#define WIEL1_INPUT         BIT0
#define ROOD_IN             BIT1
#define GROEN_IN            BIT2
#define BLAUW_IN            BIT3
#define START_IN            BIT4
#define RESET_IN            BIT5

char tijdSec[5];
char tijdMin[5];
char meter[3];
char centimeter[5];
char aantalRoodChar[3];
char aantalGroenChar[3];
char aantalBlauwChar[3];
int aantalBlokkeringen = 0; // Startwaarde voor de afstandsmeting
int aantalMeter; // Variabele voor het aantal gereden meters (aantalBlokkeringen / 100)
int min; // Variabele voor het aantal verstreken minuten
int sec; // Variabele voor het aantal verstreken seconden
int secMod; // Variabele voor (sec % 60)
int centimeterMod; // Variabele voor (centimeter % 100)
int aantalKeerNul = 0; // Variabele voor het bijhouden van het aantal maal tellen tot 32500
int aantalRood = 0; // Startwaarde voor het aantal maal gedetecteerde kleur
int aantalGroen = 0; // Startwaarde voor het aantal maal gedetecteerde kleur
int aantalBlauw = 0; // Startwaarde voor het aantal maal gedetecteerde kleur
int pinRood;
int pinGroen;
int pinBlauw;
int updateDisplay; // 1 of 0. Variabele voor wanneer het display gerefresht moet worden
int status; // Gestart (1) of gestopt (0)
int splash = 1; // Splash screen weergeven
int updateSplash;
//int noSplashOnReset = 0;
int resetPressed = 0;


#pragma vector = PORT1_VECTOR
__interrupt void aantalGaatjes(void) // Interrupt voor het tellen van de gaatjes
{                                    // en voor het bijhouden van het aantal
                                     // gedetecteerde kleuren

    if ((P1IFG & RESET_IN) != 0)
    {
        //WDTCTL = 0;
        resetPressed = 1;
    }
    P1IFG &= ~(RESET_IN);

    if ((P1IFG & START_IN) != 0)
    {
        status = !status;
        __delay_cycles(10);
        splash = 0;
        updateSplash = 1;
    }
    P1IFG &= ~(START_IN);

    pinRood = (P1IFG & ROOD_IN);
        if (pinRood != 0)
        {
            if (status == 1)
            {
                updateDisplay = 1;
                aantalRood++;
            }

        }
    P1IFG &= ~(ROOD_IN);

    pinGroen = (P1IFG & GROEN_IN);
        if (pinGroen != 0)
        {
            if (status == 1)
            {
                updateDisplay = 1;
                aantalGroen++;
            }
        }
    P1IFG &= ~(GROEN_IN);

    pinBlauw = (P1IFG & BLAUW_IN);
        if (pinBlauw != 0)
        {
            if (status == 1)
            {
                updateDisplay = 1;
                aantalBlauw++;
            }
        }
    P1IFG &= ~(BLAUW_IN);

    if (P1IN & WIEL1_INPUT)
    {
        if(status == 1)
        {
            aantalBlokkeringen = aantalBlokkeringen + 1;
            if (aantalBlokkeringen >= 32500) // In de buurt van 2^16
            {
                aantalBlokkeringen = 0; // Gemeten afstand weer op 0 zetten voor een tweede cyclus
                aantalKeerNul++; // Mogelijkheid creëren om verder dan 2^16 te tellen
            }
        }
    }
    P1IFG &= ~(WIEL1_INPUT);
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void verstrekenTijd(void) // Tijd bijhouden
{
    static int zoveelsteInterrupt = 0;
        zoveelsteInterrupt++;
        if(zoveelsteInterrupt == 32)
        {
            zoveelsteInterrupt = 0;
            if(status == 1)
            {
                sec++; // Het optellen van seconden
            }
        }
    TA0CTL &= ~TAIFG;
}


void stelTimer0In(void) // Configureer Timer0
{
    TA0CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;     // Stel Timer_0 in
    TA0CCR0 = 62500;                            // Genereert 2x p/s een interrupt
}

void captions(void) //Statische tekst die altijd op het display staat of overschreven wordt bij startsignaal
{
    oledPrint(0,4,"Kleur: ---- ", small);
    oledPrint(0, 6, "R", small);
    oledPrint(45, 6, "G", small);
    oledPrint(90, 6, "B", small);
    oledFillBox(0,5,127,5,  0x40); // Horizontale lijn
    oledFillBox(42,6,42,7,0xFF); // Seperator links
    oledFillBox(87,6,87,7,0xFF); // Seperator rechts
    oledPrint(0, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    oledPrint(45, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    oledPrint(90, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    oledPrint(0,0, "Tijd: ", small);
    oledPrint(57, 0, ":", small);
    oledPrint(0,2, "Afstand: ", small);
}

void resetButton(void) // Alle waarden op 0 zetten
{
    oledClearBox(65, 2, 127, 2);
    sec = 0;
    min = 0;
    secMod = 0;
    aantalBlokkeringen = 0;
    aantalKeerNul = 0;
    status = 0;
    centimeterMod = 0;
    aantalMeter = 0;
    aantalRood = 0;
    itoa(aantalRood, aantalRoodChar);
    aantalGroen = 0;
    itoa(aantalGroen, aantalGroenChar);
    aantalBlauw = 0;
    itoa(aantalBlauw, aantalBlauwChar);
    oledClearBox(0, 4, 127, 4);
    oledClearBox(0, 7, 40, 7);
    oledClearBox(45, 7, 85, 7);
    oledClearBox(90, 7, 127, 7);
    oledPrint(0,4,"Kleur: ---- ", small);
    oledPrint(0, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    oledPrint(45, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    oledPrint(90, 7, "0", small); // Een 0 voor de geïnitialiseerde waarde
    resetPressed = 0; // resetPressed marker 0 maken, reset is voltooid
}

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */

    P1DIR &= ~(WIEL1_INPUT | ROOD_IN | GROEN_IN | BLAUW_IN | START_IN | RESET_IN); // Inputs
    P1IES &= ~(WIEL1_INPUT | ROOD_IN | GROEN_IN | BLAUW_IN | RESET_IN); // Rising edge
    P1IES |=  (START_IN); // Falling edge
    P1IE  |=  (WIEL1_INPUT | ROOD_IN | GROEN_IN | BLAUW_IN | START_IN | RESET_IN); // Interrupt enable
    P1REN |=  (ROOD_IN | GROEN_IN | BLAUW_IN | START_IN | RESET_IN); // Pull-up/downweerstanden
    P1OUT &= ~(ROOD_IN | GROEN_IN | BLAUW_IN); // Pull-downweerstanden
    P1OUT |=  (START_IN | RESET_IN); // Pull-upweerstanden

    stelTimer0In();
    oledInitialize();    //Zet display aan
    oledSetOrientation(FLIPPED);    //flippen
    oledClearScreen();    //begin met leeg scherm

    __enable_interrupt();

    while(1)
    {
        if (splash == 1)// && (noSplashOnReset == 0))
        {
            oledPrint(22,0,"Spaghetti", big);
            oledPrint(27, 3,"monster", big);
            oledPrint(6, 7, "Druk op START...", small);
            __delay_cycles(7000000);
            oledClearBox(0, 7, 127, 7);
            __delay_cycles(3000000);
        }
        if ((splash != 1))
        {
            if (updateSplash != 0)
            {
                oledClearScreen();
                captions(); // Alle statische tekst weergeven
                updateSplash = 0;
                //noSplashOnReset = 1;
            }

            if (resetPressed == 1)
            {
                resetButton();
            }

            if (status == 1)
            {
                oledPrint(118, 0, "!", small);
            }

            if (status != 1)
            {
                oledClearBox(118, 0, 127, 0);
            }

    //-------------------------Tijd--------------------------------

            secMod = sec % 60; // Seconden bij 60s weer vanaf 0 laten tellen
            min = (sec / 60) % 60; // Minuten bij 60m weer vanaf 0 laten tellen
            itoa(secMod, tijdSec); //int 'secMod' naar char 'tijdSec'
            itoa(min, tijdMin); //int 'min' naar char 'tijdMin'

            if (min <= 9) // Zorgt ervoor dat er een leading zero wordt geplaatst
            {
                oledPrint(48,0, tijdMin, small);
                oledPrint(39,0, "0", small);
            }
            else if (min > 9)
            {
                oledPrint(39,0, tijdMin, small);
            }
            if (secMod <= 9)
            {
                oledPrint(70, 0, tijdSec, small);
                oledPrint(61, 0, "0", small);
            }
            else if (secMod > 9)
            {
                oledPrint(61, 0, tijdSec, small);
            }



    //-------------------------Afstand--------------------------------
            aantalMeter = ((aantalBlokkeringen / 100) + (325 * aantalKeerNul)); //tellen boven 16-bit range
            centimeterMod = aantalBlokkeringen % 100;
            itoa(aantalMeter, meter);
            itoa(centimeterMod, centimeter);
            if (aantalMeter <= 999) // Voorkomen dat er meer dan vijf digits worden weergegeven
            {
                uint8_t lastAfstand = oledPrint(65, 2, meter, small);
                oledPrint((lastAfstand + 65), 2, ".", small);
                oledPrint((lastAfstand + 87), 2, "m", small);
                if (centimeterMod < 10) // Zorgt ervoor dat er een leading zero wordt geplaatst
                {
                    oledPrint((lastAfstand + 78), 2, centimeter, small);
                    oledPrint((lastAfstand + 69), 2, "0", small);
                }
                else if (centimeterMod >= 10)
                {
                    oledPrint((lastAfstand + 69), 2, centimeter, small);
                }
                if ((aantalMeter % 10 == 0) && (aantalMeter == 10) && (centimeterMod >= 0) && (centimeterMod <= 10))
                {
                    oledClearBox(81, 2, 82, 2); // Refresh display bij het verschuiven van de komma bij 10m
                    __delay_cycles(110000); // Tekst niet laten flikkeren
                }
                if ((aantalMeter % 10 == 0) && (aantalMeter == 100) && (centimeterMod >= 0) && (centimeterMod <= 10))
                {
                    oledClearBox(90, 2, 91, 2); // Refresh display bij het verschuiven van de komma bij 100m
                    __delay_cycles(110000); // Tekst niet laten flikkeren
                }
            }
            else
            {
                oledClearBox(65, 2, 127, 2);
                oledPrint(65, 2, " EXCEED ", small); // Weergeven boven 999m
            }

    //-------------------------Kleur--------------------------------

            oledPrint(0, 7, aantalRoodChar, small); //Aantal keer rood weergeven
            oledPrint(45, 7, aantalGroenChar, small); //Aantal keer groen weergeven
            oledPrint(90, 7, aantalBlauwChar, small); //Aantal keer blauw weergeven
            if ((pinRood != 0) && (updateDisplay != 0)) //als kleur == rood
            {
                itoa(aantalRood, aantalRoodChar); //int 'kleurRood' naar char 'rood'
                oledClearBox(48, 4, 127, 4);
                oledPrint(48, 4, "Rood", small);
                updateDisplay = 0;
            }
            else if ((pinGroen != 0) && (updateDisplay != 0)) //als kleur == groen
            {
                itoa(aantalGroen, aantalGroenChar); //int 'kleurGroen' naar char 'groen'
                oledClearBox(48, 4, 127, 4); // Display updaten
                oledPrint(48, 4, "Groen", small);
                updateDisplay = 0; // Mogelijkheid creëren tot het opnieuw updaten van het display
            }
            else if ((pinBlauw != 0) && (updateDisplay != 0)) //als kleur == blauw
            {
                itoa(aantalBlauw, aantalBlauwChar); //int 'kleurBlauw' naar char 'blauw'
                oledClearBox(48, 4, 127, 4); // Display updaten
                oledPrint(48, 4, "Blauw", small);
                updateDisplay = 0; // Mogelijkheid creëren tot het opnieuw updaten van het display
            }
        }
    }
    return 0;
}
